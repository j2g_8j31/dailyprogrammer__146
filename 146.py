#!/usr/bin/python

from math import *

sides, circumRadius = raw_input().split()
sides = float(sides)
circumRadius = float(circumRadius)

perimeter = sides * circumRadius * 2.0 * sin(pi / sides)
print "%0.3f" % perimeter
